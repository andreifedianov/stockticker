package com.andreifedianov.java.bubbletea;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class StockTicker {
	String[] array;

	private HashMap<String, Integer> m = new HashMap<String, Integer>();



	public void run(String[] arr){
		this.array = arr;
		System.out.println("Input:" + arr.toString());
		gather(array);
		System.out.println("Gather:" + m.toString());
		printMap(sortMapByValues(m));
	}

	private void gather(String[] args){
		for(String arg : args){
			if (!m.containsKey(arg.toString())){
				m.put(arg, 1);
			} else {
				m.put(arg, (int) m.get(arg) + 1);
			}
		}
	}

	private Map<String, Integer> sortMapByValues(Map<String, Integer> unsortedMap){
		List<Map.Entry<String, Integer>> list = new LinkedList<Map.Entry<String, Integer>>(unsortedMap.entrySet());
		Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
			@Override
			public int compare(Entry<String, Integer> o1, Entry<String, Integer> o2) {
				return o1.getKey().compareTo(o2.getKey());
			}
		});
		Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
			@Override
			public int compare(Entry<String, Integer> o1, Entry<String, Integer> o2) {
				return o2.getValue() - o1.getValue();
			}
		});
		Map<String, Integer> result = new LinkedHashMap<String, Integer>();
		for(Map.Entry<String,Integer> entry : list){
			result.put(entry.getKey(), entry.getValue());
		}

		return result;
	}

	private void printMap(Map<String, Integer> input){
		System.out.println("Raw:"+input.toString());
		for(Map.Entry<String, Integer> entry : input.entrySet()){
			System.out.println("Key:" + entry.getKey() + "\tValue:" + entry.getValue());
		}
	}


}
