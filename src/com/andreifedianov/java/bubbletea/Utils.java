package com.andreifedianov.java.bubbletea;
public class Utils {

	private Utils(){}

	 static public boolean empty(String str) {
	        return str == null ? true : str.trim().length() == 0;
	    }
	 
	 static public boolean empty(String[] str){
		 return str == null ? true : str.length == 0;
	 }

}
